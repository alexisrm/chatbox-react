import Rebase from 're-base'
import firebase from  'firebase/app'
import 'firebase/database'

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyBhmFekVOwNhAu516qQmrLdzlNHb4dEpQw",
    authDomain: "chatbox-65359.firebaseapp.com",
    databaseURL: "https://chatbox-65359.firebaseio.com"
})

const base  = Rebase.createClass(firebase.database())

export { firebaseApp }

export default base