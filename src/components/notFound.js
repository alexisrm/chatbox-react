import React from 'react'

const notFound = () => (
    <h2 className='notFound'>Page not found</h2>
)

export default notFound