import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'

class Connect extends Component {
    // Gerer un form ou un input 
    state = {
        pseudo: '',
        goToChat: false
    }

    // Mettre à jour l'input
    handleChange = event => {
        const pseudo = event.target.value
        this.setState({ pseudo })
    }

    // Mettre à jour la route après submit du form 
    handleSubmit = event => {
        event.preventDefault()
        this.setState({ goToChat: true})
    }

    render() {
        // Si le form est submit et est valide alors tu rediriges  vers la route
        if(this.state.goToChat) {
            return <Redirect push to={`/pseudo/${this.state.pseudo}`}></Redirect>
        }

        return (
            <div className="connexionBox">
                <form className="connexion" onSubmit={this.handleSubmit}>
                    <input
                        value={this.state.pseudo}
                        onChange={this.handleChange}
                        type="text"
                        placeholder="Pseudo.."
                        required
                    />
                    <button type="submit">Connexion</button>
                </form>
            </div>
        )
    }
}

export default Connect


